(function($) {
  Drupal.behaviors.etpiTranslationModal = {
    attach: function (context, settings) {
      $('.entity-translation-panels-ipe a').attr('target', '_blank');
    }
  };
})(jQuery);
